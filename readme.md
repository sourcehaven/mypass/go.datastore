# MyPass Data Storage Application

## Install packages

```bash
go mod tidy
```

## Important !!!

This service should always run on localhost, and should not listen on any other interface.
If you should intend to allow connection outside localhost, you should make the grpc server secure!

## Scripts

### To run the service you can simply execute the following command:

```bash
go run ./cmd/mypass
```

This will run the [mypass/main](./cmd/mypass/main.go) go command file.

### Client example

```bash
go run ./cmd/client
```

See the [client/main](./cmd/client/main.go) for more information on the client application.
