package srv

import (
	"context"
	"fmt"
	"github.com/bufbuild/protovalidate-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.datastore/v0-alpha/internal/data"
	datav0 "gitlab.com/sourcehaven/mypass/go.datastore/v0-alpha/internal/proto/data/v0alpha"
	gormv1 "gitlab.com/sourcehaven/mypass/go.datastore/v0-alpha/internal/proto/gorm/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

type Config struct {
	Host string   // specifies host - should be localhost !!! (no 0.0.0.0)
	Port uint16   // specifies the port to run the application on
	Dao  data.Dao // stores a valid database connection
}

type service struct {
	datav0.UnimplementedByteVaultServiceServer         // grpc service must implement this
	Cfg                                        *Config // stores service specific configs
}

type gracefulService struct {
	*service // same as service, but with graceful shutdown
}

type Service interface {
	datav0.ByteVaultServiceServer   // basic handlers
	WithConfig(c ...Config) Service // lazy config loader
	Graceful() GracefulService      // make this service a graceful service
	Start()                         // start serving the application
}

type GracefulService interface {
	Service
}

func New(c ...Config) Service {
	if len(c) == 0 {
		return &service{}
	}
	cfg := &c[0]
	// Hard coding localhost
	cfg.Host = "localhost"
	return &service{Cfg: cfg}
}

func (s *service) WithConfig(c ...Config) Service {
	if len(c) == 0 {
		return &service{}
	}
	cfg := &c[0]
	// Hard coding localhost
	cfg.Host = "localhost"
	return &service{Cfg: cfg}
}

func (s *service) Sync(ctx context.Context, c *datav0.SyncRequest) (r *emptypb.Empty, err error) {
	var val *protovalidate.Validator
	if val, err = protovalidate.New(); err != nil {
		return nil, status.Error(codes.Internal, "could not create validator")
	}
	if err = val.Validate(c); err != nil {
		return nil, status.Error(codes.FailedPrecondition, "validation failed")
	}

	logrus.WithFields(logrus.Fields{
		"task": "service.Sync",
	}).Debug("synchronizing datastore")
	dao := s.Cfg.Dao
	uid, err := strconv.ParseUint(c.Uid, 10, 64)
	if err != nil {
		return nil, status.Error(codes.Internal, "could not parse uid")
	}
	v := &data.Vault{Uid: uint(uid), Data: c.Data}
	if err = dao.Save(v); err != nil {
		logrus.WithFields(logrus.Fields{
			"task": "service.Sync",
			"err":  fmt.Sprintf("%v", err),
		}).Debug("synchronizing datastore")
		return nil, status.Error(codes.Internal, "could not save vault")
	}
	logrus.WithFields(logrus.Fields{
		"task": "service.Sync",
	}).Debug("saving vault is successful")
	return &emptypb.Empty{}, nil
}

func (s *service) Fetch(ctx context.Context, c *datav0.FetchRequest) (r *datav0.FetchResponse, err error) {
	var val *protovalidate.Validator
	if val, err = protovalidate.New(); err != nil {
		return nil, status.Error(codes.Internal, "could not create validator")
	}
	if err = val.Validate(c); err != nil {
		return nil, status.Error(codes.FailedPrecondition, "validation failed")
	}

	logrus.WithFields(logrus.Fields{
		"task": "service.Fetch",
	}).Debug("fetching datastore")
	dao := s.Cfg.Dao
	v := &data.Vault{}
	uid, err := strconv.ParseUint(c.Uid, 10, 64)
	if err != nil {
		return nil, status.Error(codes.Internal, "could not parse uid")
	}
	if v, err = dao.Fetch(uint(uid)); err != nil {
		logrus.WithFields(logrus.Fields{
			"task": "service.Sync",
			"err":  fmt.Sprintf("%v", err),
		}).Debug("fetching datastore")
		return nil, status.Error(codes.NotFound, "could not fetch vault entry")
	}
	logrus.WithFields(logrus.Fields{
		"task": "service.Fetch",
	}).Debug("fetching vault is successful")
	return &datav0.FetchResponse{Data: v.Data, Meta: toProtoMeta(&v.Model)}, nil
}

func (s *service) Graceful() GracefulService {
	return &gracefulService{service: s}
}

func (s *service) Start() {
	gs := grpc.NewServer()
	datav0.RegisterByteVaultServiceServer(gs, s)

	host := fmt.Sprintf("%s:%d", s.Cfg.Host, s.Cfg.Port)
	listener, err := net.Listen("tcp", host)
	if err != nil {
		panic(err)
	}

	// Run the service.
	logrus.Infof("starting local service on %s", s.Cfg.Host)
	logrus.WithFields(logrus.Fields{
		"topic": "service lifecycle",
		"error": fmt.Sprintf("%v", gs.Serve(listener)),
	}).Fatal("service stopped unexpectedly")
}

func (s *gracefulService) Start() {
	gs := grpc.NewServer()
	datav0.RegisterByteVaultServiceServer(gs, s)

	host := fmt.Sprintf("%s:%d", s.Cfg.Host, s.Cfg.Port)
	listener, err := net.Listen("tcp", host)
	if err != nil {
		panic(err)
	}

	// Insert channel for idle channel connections.
	idle := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
		<-sigint

		// Received an interrupt signal, shutdown.
		logrus.WithFields(logrus.Fields{
			"topic": "graceful shutdown",
		}).Info("service is shutting down")
		gs.GracefulStop()

		close(idle)
	}()

	// Run the service.
	logrus.Infof("starting graceful service on %s", host)
	logrus.WithFields(logrus.Fields{
		"topic": "service lifecycle",
		"error": fmt.Sprintf("%v", gs.Serve(listener)),
	}).Fatal("service stopped unexpectedly")

	<-idle
}

func toProtoMeta(m *gorm.Model) *gormv1.Meta {
	return &gormv1.Meta{
		Id: uint64(m.ID), CreatedAt: timestamppb.New(m.CreatedAt),
		DeletedAt: &gormv1.NullTime{
			Time:  timestamppb.New(m.DeletedAt.Time),
			Valid: m.DeletedAt.Valid,
		},
		UpdatedAt: timestamppb.New(m.UpdatedAt),
	}
}
