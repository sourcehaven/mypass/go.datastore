package cfg

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
)

// Constants for application environment configuration
const (
	ENVIRON  = "MP_DATASTORE_ENV"      // env mode e.g.: production, development, staging, testing, etc...
	PORT     = "MP_DATASTORE_PORT"     // application port e.g.: 8082, 5785, etc...
	LOGLEVEL = "MP_DATASTORE_LOGLEVEL" // logrus loglevel e.g.: fatal, error, warn, info, debug, trace
	DBCONN   = "MP_DATASTORE_DB"       // db connection uri
)

// LcEnv stores local environment variables.
type LcEnv struct {
	Port     uint16
	LogLevel logrus.Level
	DBConn   string
}

func ParseEnv() (e LcEnv, err error) {
	// Get current environment config
	cenv := os.Getenv(ENVIRON)
	if cenv == "" {
		cenv = "development"
	}
	if godotenv.Load(".env."+cenv) != nil {
		logrus.WithFields(logrus.Fields{
			"topic": "environment",
			"err":   fmt.Sprintf("%v", err),
		}).Warnf("Failed to load .env.%s", cenv)
	}

	// Load default .env file
	err = godotenv.Load()
	if err != nil {
		return
	}

	// Parse port configuration
	var port uint64
	if port, err = strconv.ParseUint(os.Getenv(PORT), 10, 16); err != nil {
		logrus.WithFields(logrus.Fields{
			"topic": "environment",
			"err":   fmt.Sprintf("%v", err),
		}).Warnf("failed to parse %s -> default :8082 will be used", PORT)
	}
	e.Port = uint16(port)

	// Parse logger level
	switch os.Getenv(LOGLEVEL) {
	case "fatal":
		e.LogLevel = logrus.FatalLevel
		break
	case "error":
		e.LogLevel = logrus.ErrorLevel
		break
	case "warn":
		e.LogLevel = logrus.WarnLevel
		break
	case "info":
		e.LogLevel = logrus.InfoLevel
		break
	case "debug":
		e.LogLevel = logrus.DebugLevel
		break
	case "trace":
		e.LogLevel = logrus.TraceLevel
		break
	}

	// Parse db uri
	e.DBConn = os.Getenv(DBCONN)

	return
}
