package data

import (
	"crypto/rand"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"slices"
	"testing"
)

func randBytes(n int) []byte {
	b := make([]byte, n)
	if _, err := rand.Read(b); err != nil {
		return nil
	}
	return b
}

var testObjects = []Vault{
	{Uid: 1, Data: randBytes(8)},
	{Uid: 2, Data: randBytes(128)},
	{Uid: 4, Data: randBytes(64)},
	{Uid: 8, Data: randBytes(4)},
	{Uid: 16, Data: randBytes(16)},
	{Uid: 32, Data: randBytes(32)},
}

type res struct {
	db *gorm.DB
}

func setup(t *testing.T) *res {
	db := Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&Vault{}))
	for _, v := range testObjects {
		assert.NoError(t, db.Create(&v).Error)
	}
	return &res{db}
}

func teardown(t *testing.T, r *res) {
	assert.NoError(t, r.db.Exec("DROP TABLE IF EXISTS vaults;").Error)
}

func TestVaultDao_Save(t *testing.T) {
	r := setup(t)
	defer teardown(t, r)

	dao := New(Config{DB: r.db})

	uid := uint(42)
	data := make([]byte, 100)
	for i := range data {
		data[i] = byte(i)
	}
	m := &Vault{Uid: uid, Data: data}
	var before int64
	var after int64
	assert.NoError(t, r.db.Model(&Vault{}).Count(&before).Error)
	assert.NoError(t, dao.Save(m))
	assert.NoError(t, r.db.Model(&Vault{}).Count(&after).Error)
	assert.Equal(t, before+1, after)
	slices.Reverse(data)
	m = &Vault{Uid: uid, Data: data}
	assert.NoError(t, dao.Save(m))
	assert.NoError(t, r.db.Model(&Vault{}).Count(&after).Error)
	assert.Equal(t, before+1, after)
	var q Vault
	assert.NoError(t, r.db.Where(&Vault{Uid: uid}).First(&q).Error)
	assert.Equal(t, data, q.Data)

	var vaults []Vault
	r.db.Find(&vaults)
	assert.Len(t, vaults, 7)
}

func TestVaultDao_Fetch(t *testing.T) {
	r := setup(t)
	defer teardown(t, r)

	dao := New(Config{DB: r.db})
	for _, o := range testObjects {
		model, err := dao.Fetch(o.Uid)
		assert.NoError(t, err)
		assert.Equal(t, o.Data, model.Data)
		assert.Equal(t, o.Uid, model.Uid)
	}
}

func TestVaultDao_Count(t *testing.T) {
	r := setup(t)
	defer teardown(t, r)

	dao := New(Config{DB: r.db})
	c, err := dao.Count()
	var expected int64
	r.db.Model(&Vault{}).Count(&expected)
	assert.NoError(t, err)
	assert.Equal(t, expected, c)
}
