package data

import (
	"errors"
	"gorm.io/gorm"
)

var ErrMissingUID = errors.New("vault save error :: model does not contain user id (uid) field")

type dao struct {
	*gorm.DB
}

type Config struct {
	DB *gorm.DB
}

type Dao interface {
	Save(m *Vault) error            // persists given entity if not found by uid, otherwise it will update it
	Fetch(uid uint) (*Vault, error) // fetches vault by corresponding user id
	Count() (int64, error)          // returns number of records in db
}

func New(c ...Config) Dao {
	if len(c) == 0 {
		c = []Config{}
	}
	conf := c[0]
	return &dao{conf.DB}
}

func (d *dao) Save(m *Vault) (err error) {
	if m.Uid == 0 {
		err = ErrMissingUID
		return
	}
	return d.Where(&Vault{Uid: m.Uid}).Assign(&Vault{Data: m.Data}).FirstOrCreate(m).Error
}

func (d *dao) Fetch(uid uint) (data *Vault, err error) {
	tmp := &Vault{}
	if err = d.Where(&Vault{Uid: uid}).First(tmp).Error; err == nil {
		data = tmp
	}
	return
}

func (d *dao) Count() (c int64, err error) {
	err = d.Model(&Vault{}).Count(&c).Error
	return
}
