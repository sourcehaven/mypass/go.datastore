package data

import "gorm.io/gorm"

type Vault struct {
	gorm.Model
	Uid  uint `gorm:"unique"`
	Data []byte
}
