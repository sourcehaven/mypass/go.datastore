package data

import (
	"database/sql"
	"fmt"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func Connect(conn string, c ...*gorm.Config) (db *gorm.DB) {
	// Prepare default config if empty
	conf := &gorm.Config{}
	if len(c) > 0 {
		conf = c[0]
	}

	// Connect with specific driver
	dbConn, err := sql.Open("sqlite3", conn)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"topic": "db connection",
			"error": fmt.Sprintf("%v", err),
		}).Panic("failed to connect to database")
	}
	// Try creating database connection with specific dialect
	if db, err = gorm.Open(sqlite.Dialector{Conn: dbConn}, conf); err != nil {
		logrus.WithFields(logrus.Fields{
			"topic": "db connection",
			"error": fmt.Sprintf("%v", err),
		}).Panic("failed to open database connection")
	}
	// Create migrations
	if err = db.AutoMigrate(&Vault{}); err != nil {
		logrus.WithFields(logrus.Fields{
			"topic": "db migration",
			"error": fmt.Sprintf("%v", err),
		}).Panic("failed to create db migration")
	}
	return
}
