package main

import (
	"context"
	"github.com/sirupsen/logrus"
	datav0 "gitlab.com/sourcehaven/mypass/go.datastore/v0-alpha/internal/proto/data/v0alpha"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// NewVaultClient creates a byte vault service client,
// that you can use in your applications.
func NewVaultClient() datav0.ByteVaultServiceClient {
	conn, err := grpc.NewClient(
		"localhost:8082",
		grpc.WithTransportCredentials(insecure.NewCredentials()), // no need for secure credentials on localhost
	)
	if err != nil {
		logrus.Fatalf("failed to dial server with %v", err)
	}
	return datav0.NewByteVaultServiceClient(conn)
}

// A minimal client application example.
func main() {
	logrus.SetLevel(logrus.DebugLevel)
	// Create client handler
	client := NewVaultClient()

	// Your desired test user id
	uid := "31"
	// Make a sync request
	vault := &datav0.SyncRequest{Data: make([]byte, 32), Uid: uid}
	if _, err := client.Sync(context.Background(), vault); err != nil {
		logrus.Fatalf("failed to sync data with %v", err)
	}

	// Fetch data
	var dbVault *datav0.FetchResponse
	dbVault, err := client.Fetch(context.Background(), &datav0.FetchRequest{Uid: uid})
	if err != nil {
		logrus.Fatalf("failed to fetch data with %v", err)
	}

	logrus.WithFields(logrus.Fields{
		"data": dbVault.Data,
	}).Debug("debugging data store fetch api")
}
