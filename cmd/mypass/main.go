package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.datastore/v0-alpha/internal/cfg"
	"gitlab.com/sourcehaven/mypass/go.datastore/v0-alpha/internal/data"
	"gitlab.com/sourcehaven/mypass/go.datastore/v0-alpha/internal/srv"
)

func main() {
	// Parse local environment configurations
	env, err := cfg.ParseEnv()
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"topic": "server initialization",
			"err":   fmt.Sprintf("%v", err),
		}).Warn("failed to parse environment config")
	}
	logrus.SetLevel(env.LogLevel)

	// Starting service
	srv.New().WithConfig(srv.Config{
		Port: env.Port,
		Dao: data.New(data.Config{
			DB: data.Connect(env.DBConn),
		}),
	}).Graceful().Start()
}
